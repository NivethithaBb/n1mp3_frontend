import { NgModule, Component, OnInit, ViewChild, enableProdMode } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import { HttpService } from '../../shared/services';
import { ResultService } from '../../shared/services/result.service';
import { Router } from '@angular/router';
import * as sha512 from 'js-sha512';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../src/environments/environment';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  ngOnInit() {
    
  }

  @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent
  customer: any;
  customerDataSend:any;
  colCountByScreen: object;
  password = "";
  passwordOptions: any = {
      mode: "password",
      value: this.password
  };

  buttonOptions1: any ={
    text: "Register",
    type: "success",
    disabled: true,
    useSubmitBehavior: true
  };
  buttonOptions2: any ={
    text: "Register",
    type: "success",
    disabled: false,
    useSubmitBehavior: true
  };
  cancelOptions: any = {
    text: "Cancel",
    type: "cancel",
    useSubmitBehavior: false
  }
  // passwordPattern: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
  passwordPattern: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@~!#$%^&*()\-=_+;:"'?])[A-Za-z\d@~!#$%^&*()\-=_+;:"'?]{8,}$/;

  //apiURL: string = 'https://connect.opuspark.com.au/1mp/api/User/Register'
  // apiURL: string ='https://localhost:44344/api/User/Register'
  apisource:string='api/User/Register'
  apiURL: string = environment.apiUrl+this.apisource
  
  constructor(private http: HttpService, private router: Router, public result: ResultService) {
    this.customer = {};
    this.customerDataSend = {};
    this.colCountByScreen = {
      xs: 1,
      sm: 2,
      md: 2,
      lg: 2
    };
  }
  popupVisible = false;
  popupMessage:string;
  popupTitle:string;
  termsAndConds = false;

  submitForm(e){
    this.result.loading = true;
    this.customerDataSend = {
      firstName: this.customer.FirstName,
      lastName:  this.customer.LastName,
      mobile: this.customer.Mobile,
      licePlate: this.customer.licePlate,
      email:this.customer.Email,
      termsAndConds:true,
      password: sha512.sha512(this.customer.password)
    }

    // setTimeout(function(){
      this.http.postData(this.apiURL, this.customerDataSend).subscribe((data) => {
        // this.result.loading = false;
        this.result.loading = false;
        if(data.status == 'Success'){
          notify({
            position: { at: 'top centre', my: 'top centre', offset: '0 250'},
            message:data.message
          }, "success", 2000);
          // this.showInfo(data.message, data.status);
          console.log(data.results);
          this.customer = {};
          if(this.result.notMobileApp){
            setTimeout(function(){
              this.router.navigate(['/login']);
            }, 4000);
          }
        } else {
          notify({
            position: { at: 'top centre', my: 'top centre', offset: '0 250'},
            message:data.message
          }, "warning", 2000);
          // this.showInfo(data.message, data.status);
        }
        (err) => console.log(err)
      });
    // }, 0);

    console.log(this.customerDataSend);

    e.preventDefault();
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(function(){
    this.replaceText('div', 'I agree','You not agree')
    }, 2000)
  }


  // showInfo(message?,title?) {
  //   this.popupMessage = message;
  //   this.popupTitle = title;
  //   this.popupVisible = true;
  // }

  showInfo(e) {
    // this.popupMessage = message;
    // this.popupTitle = title;
    this.popupVisible = true;
    e.preventDefault();

  }

  emailComparison = () => {
    return this.customer.Email
  };

  passwordComparison = () => {
    return this.customer.password
  };
  checkComparison() {
      return true;
  }
}
