import { Component, OnInit } from '@angular/core';
import { ResultService } from '../../shared/services/result.service';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CreditCardComponent implements OnInit {

  backlink:string = '/home'
  constructor(public result: ResultService) { }

  ngOnInit() {
  
    if(!this.result.notMobileApp){
      this.backlink = '/app/search';
      // console.log(this.backlink);
    }
  }


}
