import { NgModule, Component, OnInit, ViewChild, enableProdMode } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import { HttpService } from '../../shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, AppInfoService } from '../../shared/services'
import { ResultService } from '../../shared/services/result.service';
import * as sha512 from 'js-sha512';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../src/environments/environment';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {

  ngOnInit() {
  }

  // @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent
  customer: any;
  customerDataSend:any;
  colCountByScreen: object;

  buttonOptions: any = {
    text: "VERIFY",
    type: "success",
    useSubmitBehavior: true
  }
apisource:string=environment.apiUrl;
  // apiURL: string = 'https://connect.opuspark.com.au/1mp/api/User/GetUser'
  // apiVerifyURL: string = 'https://connect.opuspark.com.au/1mp/api/User/VerifyUser'
  apisourceURL:string='api/User/GetUser';
  apisourceVerifyURL :string ='api/User/VerifyUser';
  apiURL: string = environment.apiUrl+this.apisourceURL;
  apiVerifyURL: string = environment.apiUrl+this.apisourceVerifyURL;

  constructor(private http: HttpService, private authService: AuthService, private router: Router,  private activateRoute: ActivatedRoute, public result: ResultService) {
    this.customer = {};
    this.customerDataSend = {};
    this.colCountByScreen = {
      xs: 1,
      sm: 1,
      md: 1,
      lg: 1
    };
    this.activateRoute.params.subscribe(params => {
    this.result.loading = true;
      this.http.postData(this.apiURL, this.customer, params['token']).subscribe((data) => {
        this.result.loading = false;
        if(data.status == 'Success'){
          this.customer = data.results;
        } else {
          notify({
            position: { at: 'top centre', my: 'top centre', offset: '0 250'},
            message:data.message
          }, "warning", 2000);
          // this.showInfo(data.message, data.status);
        }
        (err) => console.log(err)
      });
      
   });
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
      // this.result.loading = false;
    
  }
  popupVisible = false;
  popupMessage:string;
  popupTitle:string;


  submitForm(e){
    // this.router.navigate(['/login']);
    this.result.loading = true;
    this.customerDataSend = {
      token: this.customer.token,
      verifyDigits: this.customer.verification
    }
    console.log(this.customerDataSend)

    this.http.postData(this.apiVerifyURL, this.customerDataSend, this.customer.token).subscribe((data) => {
      this.result.loading = false;
      if(data.status == 'Success'){
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "success", 3000);

        setTimeout( () => {
          this.router.navigate(['/login']);
        }, 3020);
        // this.showInfo(data.message, data.status, true);
        // this.customer = {};
        console.log(data.results);
      } else {
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "warning", 3000);
        // this.showInfo(data.message, data.status);
      }
      (err) => console.log(err)
    });

    e.preventDefault();
  }

  redirect(){
    this.router.navigate(['/login']);
  }
  // setTimeout(function(){
  //   this.redirect();

  // }, 2000);

  showInfo(message,title, redirect?) {
    this.popupMessage = message;
    this.popupTitle = title;
    this.popupVisible = true;
    if(redirect){
      setTimeout( () => {
        this.router.navigate(['/login']);

      }, 3000);
     
    }
  }

  passwordComparison = () => {
    return this.customer.password
  };
  checkComparison() {
      return true;
  }
}
