import { NgModule, Component, OnInit, ViewChild, enableProdMode } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import { HttpService } from '../../shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import { ResultService } from '../../shared/services/result.service';
import * as sha512 from 'js-sha512';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../src/environments/environment';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.scss']
})
export class RecoveryComponent implements OnInit {

  ngOnInit() {
  }

  @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent
  customer: any;
  customerDataSend:any;
  colCountByScreen: object;
  showBack:boolean = true;

  buttonOptions: any = {
    text: "Send",
    type: "success",
    useSubmitBehavior: true
  }

  options:any = {
    disabled: false,
    onValueChanged: e => {
      this.changeEmail(e.value)
    }
    // mask: '^[0-9_.-]*$',
    // useMaskedValue: true
  };

 // apiURL: string = 'https://connect.opuspark.com.au/1mp/api/User/ResetPassword'
  // apiURL: string = 'https://localhost:44344/api/User/ResetPassword'
  apisource:string='api/User/ResetPassword';
  apiURL: string = environment.apiUrl+this.apisource;
  constructor(private http: HttpService, public result: ResultService, private router: Router,  private activateRoute: ActivatedRoute) {
    this.customer = {};
    this.customerDataSend = {};
    this.colCountByScreen = {
      xs: 1,
      sm: 1,
      md: 1,
      lg: 1
    };
  }
  popupVisible = false;
  popupMessage:string;
  popupTitle:string;

  changeEmail(email){
   //  console.log(number)
    email = email.replaceAll(' ','')
    this.customer.Email = email;
  }

  submitForm(e){
    this.result.loading = true;
    this.customerDataSend = {
      email: this.customer.Email
    }


      this.http.postData(this.apiURL, this.customerDataSend).subscribe((data) => {
        // this.result.loading = false;
          this.customer = {};
        if(data.status == 'Success'){
          notify({
            position: { at: 'top centre', my: 'top centre', offset: '0 250'},
            message:data.message
          }, "success", 2000);
          // this.showInfo(data.message, data.status);
          // console.log(data.results);
          // this.router.navigate(['/login']);
        } else {
          notify({
            position: { at: 'top centre', my: 'top centre', offset: '0 250'},
            message:data.message
          }, "warning", 2000);
          // this.showInfo(data.message, data.status);
        }
        this.result.loading = false;
        (err) => console.log(err)
      });

    console.log(this.customerDataSend);

    e.preventDefault();
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    
  }

  showInfo(message,title) {
    this.popupMessage = message;
    this.popupTitle = title;
    this.popupVisible = true;
  }

  passwordComparison = () => {
    return this.customer.password
  };
  checkComparison() {
      return true;
  }
}
