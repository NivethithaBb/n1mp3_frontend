import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../shared/services';
import { ResultService } from '../../shared/services/result.service';
import { Router, ActivatedRoute } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { stringify } from '@angular/compiler/src/util';
import { environment } from './../../../../src/environments/environment';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
})
export class ConfirmationComponent implements OnInit {
  public show: boolean = false;
  results: any = [];
  confirmedResults: any = [];
  confirmedData: any = [];
  public proid: string;
  backlink: string = '/home';
  // private apiURL:string = 'https://connect.opuspark.com.au/1mp/';
  //private apiURL:string = 'https://localhost:44344/';
  private apiURL: string = environment.apiUrl;

  constructor(
    private http: HttpService,
    private router: Router,
    private activateRoute: ActivatedRoute,
    public result: ResultService
  ) {}

  ngOnInit() {
    this.result.loading = true;
    // console.log('this.result.notMobileApp');
    // console.log(this.result.notMobileApp);

    if (!this.result.notMobileApp) {
      this.backlink = '/app/search';
      // console.log(this.backlink);
    }

    if (this.result.searchResult) {
      if (this.result.calendarValue === 1) {
        var gettingSaveurl = this.result.searchResult;
        var sbuldingid = this.result.Buildingid;
        this.proid = this.result.calendarValue;

        for (let value of gettingSaveurl) {
          if (value.BuildingId == sbuldingid) {
            const apiURL =
              this.apiURL +
              value.SaveUrl +
              '&cs=' +
              this.result.sessionID +
              '&LicPlNo=' +
              value.LicPlateClass;
            this.http.getData(apiURL).subscribe((data) => {
              this.result.loading = false;
              if (data.status == 'Success') {
                this.result.searchResult = '';
                this.confirmedData.push(data.results);
                this.confirmedResults = this.confirmedData.sort();
                this.show = true;
              } else {
                notify(
                  {
                    position: {
                      at: 'top centre',
                      my: 'top centre',
                      offset: '0 250',
                    },
                    message: data.message,
                  },
                  'warning',
                  2000
                );
                // this.router.navigate([this.home]);
              }
              (err) => {
                this.result.loading = false;
                // console.log(err)
              };
            });
          }
        }
      } else {
        this.proid = this.result.calendarValue;
        //  const apiURL = this.apiURL + value.SaveUrl + '&cs=' + this.result.sessionID+'&LicPlNo='+value.LicPlateClass;

        const apiURL =
          this.apiURL +
          this.result.searchResult[1].SaveUrl +
          '&cs=' +
          this.result.sessionID +
          '&LicPlNo=' +
          this.result.searchResult[1].LicPlateClass;
        this.http.getData(apiURL).subscribe((data) => {
          this.result.loading = false;
          if (data.status == 'Success') {
            this.result.searchResult = '';
            this.confirmedData = data.results;
            this.show = true;
          } else {
            notify(
              {
                position: {
                  at: 'top centre',
                  my: 'top centre',
                  offset: '0 250',
                },
                message: data.message,
              },
              'warning',
              2000
            );
            // this.router.navigate([this.home]);
          }
          (err) => {
            this.result.loading = false;
            // console.log(err)
          };
        });
      }

      //const apiURL = this.apiURL + this.result.searchResult.SaveUrl + '&cs=' + this.result.sessionID;
    } else {
      this.router.navigate(['/home']);
    }
  }
}
