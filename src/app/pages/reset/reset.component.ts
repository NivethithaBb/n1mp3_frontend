

import { NgModule, Component, OnInit, ViewChild, enableProdMode } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import { HttpService } from '../../shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import { ResultService } from '../../shared/services/result.service';
import * as sha512 from 'js-sha512';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../src/environments/environment';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

  ngOnInit() {
    this.result.loading = true;
   this.activateRoute.params.subscribe(params => {

    this.customer.token = params['token'];

      this.http.postData(this.apiURL, this.customer, params['token']).subscribe((data) => {
        this.result.loading = false;
        if(data.status == 'Success'){
          // notify({
          //   position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          //   message:data.message
          // }, "success", 2000);
          // console.log(data);
          // console.log(data.results);
          this.customer = data.results;
          // this.customer.Email = data.results.email;
        } else {
          notify({
            position: { at: 'top centre', my: 'top centre', offset: '0 250'},
            message:data.message
          }, "warning", 2000);
          // this.showInfo(data.message, data.status);
        }
        (err) => console.log(err)
      });
   });
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
      // this.result.loading = false;
    
  }

  @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent
  customer: any;
  customerDataSend:any;
  colCountByScreen: object;
  password = "";
  passwordOptions: any = {
      mode: "password",
      value: this.password
  };

  onHiding(e:any){
   //this.router.navigate(['/home']);
   window.close();
}

  buttonOptions: any = {
    text: "Reset",
    type: "success",
    useSubmitBehavior: true
  }
  cancelOptions: any = {
    text: "Cancel",
    type: "cancel",
    useSubmitBehavior: false
  }
  // passwordPattern: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
  passwordPattern: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@~!#$%^&*()\-=_+;:"'?])[A-Za-z\d@~!#$%^&*()\-=_+;:"'?]{8,}$/;

  //apiURL: string = 'https://connect.opuspark.com.au/1mp/api/User/GetPasswordUser';
  //apiResetURL: string = 'https://connect.opuspark.com.au/1mp/api/User/UpdatePassword'
  // apiURL: string = 'https://localhost:44344/api/User/GetPasswordUser';
  // apiResetURL: string = 'https://localhost:44344/api/User/UpdatePassword'
  apisourceurl:string ='api/User/GetPasswordUser';
  apisourceResetURL:string ='api/User/UpdatePassword';
  apiURL: string= environment.apiUrl+this.apisourceurl;
  apiResetURL: string =environment.apiUrl+this.apisourceResetURL;
  constructor(private http: HttpService, private router: Router,  private activateRoute: ActivatedRoute, public result: ResultService) {
    this.customer = {};
    this.customerDataSend = {};
    this.colCountByScreen = {
      xs: 1,
      sm: 1,
      md: 1,
      lg: 1
    };
  }
  popupVisible = false;
  popupMessage:string;
  popupTitle:string;

  submitForm(e){
    this.result.loading = true;
    this.customerDataSend = {
      token: this.customer.token,
      password: sha512.sha512(this.customer.password)
    }

    this.http.postData(this.apiResetURL, this.customerDataSend, this.customer.token).subscribe((data) => {
      setTimeout(() => {
        this.result.loading = false;
      }, 500);
      if(data.status == 'Success'){
        // notify({
        //   position: { at: 'top centre', my: 'top centre', offset: '0 250'},
        //   message:data.message
        // }, "success", 2000);
        // this.showInfo(data.message, data.status);
        console.log(data.results);
        // setTimeout( () => {
        //   this.router.navigate(['/login']);
        // }, 3020);
        this.showInfo("Password is updated. Please sign-on to the mobile APP to continue", "Reset Password");
       // this.router.navigate(['/home']);
     // this.result.loading=false;
    
       
      } else {
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "warning", 2000);
        // this.showInfo(data.message, data.status);
      }
      (err) => console.log(err)
    });

    console.log(this.customerDataSend);

    e.preventDefault();
  }

  showInfo(message,title) {
    this.popupMessage = message;
    this.popupTitle = title;
    this.popupVisible = true;
  }

  passwordComparison = () => {
    return this.customer.password
  };
  checkComparison() {
      return true;
  }
}
