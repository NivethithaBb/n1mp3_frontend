import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../shared/services';
import { ResultService } from '../../shared/services/result.service';
import { Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../src/environments/environment';
import { DxMapModule, DxResponsiveBoxModule } from 'devextreme-angular';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})
export class ResultComponent implements OnInit {
  public centerCoordinates: any;
  googleMap: any;
  index: any = [];
  results: any = [];
  BuildingResults: any = [];
  popupMessage: string;
  popupVisible = false;
  popupTitle: string;
  home: string = '/home';
  buttonText: string = 'Book Now';
  sendURL: string;
  private values: Date;
  DateDetails: any = [];
  public bounds: any;
  public proid: string;
  public licplate: string;
  private id: number;
  private BuildingIdvalue: number;
  private obj: Bindvalue;
  objarr: any = [];
  checkedDates = [];
  LicPlateNumber = [];
  notagree: boolean = false;
  licplatevisible: boolean = false;
  mapMarkers: any = [];
  latitude: any;
  longtitude: any;
  lavalue: any;
  longvalue: any;
  public res: string;
  hiddenbuilding: string;
  gimg: any;
  //clickurl:string;

  private apiURL: string = environment.apiUrl;
  //private resultAPI:string = this.apiURL + '/api/Booking/Search/';
  private resultAPI: string = this.apiURL + '/api/Booking/SearchGm/';
  private resultAPI1: string = this.apiURL + '/api/Booking/GetLicensePlate/';
  screen(width) {
    return width < 700 ? 'sm' : 'lg';
  }

  constructor(
    private http: HttpService,
    private router: Router,
    public result: ResultService,
    public _AuthService: AuthService
  ) {
    if (!_AuthService.loggedIn) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    this.result.loading = true;
    if (!this.result.notMobileApp) {
      this.home = '/app/search';
    }
    const blupng = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
    // const redpng = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
    const redpng = 'assets/images/red-dot.png';
    this.license();
    if (this.result.searchTerm) {
      var gettingSearchTerm = this.result.searchTerm;
      this.proid = gettingSearchTerm.productType;
      if (gettingSearchTerm.productType == 1) {
        this.id = 0;
        for (let value of gettingSearchTerm.startDate) {
          this.values = value;

          this.http.getData(this.formQueryURL()).subscribe((data) => {
            this.result.loading = false;
            if (data.status == 'Success') {
              for (var j = 0; j < data.results.length; j++) {
                console.log(data.results);
                this.getSendURL(data.loginRequired, data.payInfoRequired);
                this.results = data.results;
                this.obj = new Bindvalue();
                this.obj.id = this.id + 1;
                this.obj.result = data.results[j].DateInfo;
                this.obj.SaveUrl = data.results[j].SaveUrl;
                this.obj.StartDate = data.results[j].StartDate;
                this.obj.BuildingId = data.results[j].BuildingId;
                this.objarr.push(this.obj);
              }
              this.BuildingResults = data.results;
              localStorage.setItem(
                'BuildingDetails',
                JSON.stringify(this.BuildingResults)
              );
              var points = [];

              for (var i = 0; i < this.BuildingResults.length; i++) {
                var loca = {
                  location:
                    this.BuildingResults[i].Latitude.toString() +
                    ',' +
                    this.BuildingResults[i].Longitude.toString(),
                  tooltip: {
                    text:
                      '<span onclick="(function(){' +
                      'const slides = document.getElementsByClassName(' +
                      "'build'" +
                      ');' +
                      'for (let i = 0; i < slides.length; i++) {' +
                      'const slide = slides[i];' +
                      "slide.style.display = 'none';}" +
                      "var getdivbuilding = 'divbuilding" +
                      this.BuildingResults[i].BuildingId +
                      "';" +
                      "document.getElementById('sbuilding').value=" +
                      this.BuildingResults[i].BuildingId +
                      ';' +
                      "document.getElementById(getdivbuilding).style.display ='block';" +
                      '})();" style="cursor:pointer;font-weight:bold;">' +
                      this.BuildingResults[i].BuildingName.toString() +
                      '</span>',
                    isShown: true,
                  },
                  iconSrc: redpng,
                  //  this.BuildingResults[i].Radius === '0' ? redpng : blupng,
                  // size:this.mapMarkers.Size(20, 32),
                  onClick: function (e) {
                    var getobj = JSON.parse(
                      localStorage.getItem('BuildingDetails')
                    );
                    console.log(getobj);

                    this.res = getobj.filter(
                      (o) =>
                        o.Latitude === e.location.lat &&
                        o.Longitude === e.location.lng
                    )[0].BuildingId;
                    (
                      document.getElementById('sbuilding') as HTMLInputElement
                    ).value = this.res;

                    /*if(this.res=='1')
                        {

                         // document.getElementById("divbuilding0").style.display = 'block';
                          // document.getElementById("divbuilding1").style.display = 'none';

                        }
                        else if(this.res=='2') {

                         // document.getElementById("divbuilding1").style.display = 'block';
                          // document.getElementById("divbuilding0").style.display = 'none';

                        }
                        else {

                          document.getElementById("divbuilding0").style.display = 'none';
                          document.getElementById("divbuilding1").style.display = 'none';

                        }*/

                    const slides = document.getElementsByClassName('build');
                    for (let i = 0; i < slides.length; i++) {
                      const slide = slides[i] as HTMLElement;
                      slide.style.display = 'none';
                    }
                    var getdivbuilding = 'divbuilding' + this.res;
                    document.getElementById(getdivbuilding).style.display =
                      'block';

                    setTimeout(function () {
                      document.querySelector('#target').scrollIntoView({
                        behavior: 'smooth',
                        block: 'end',
                        inline: 'nearest',
                      });
                    }, 1000);
                  },
                };

                this.mapMarkers.push(loca);
              }

              this.obj.LicPlateClass = this.licplate;
              this.DateDetails.push(this.obj);
              this.checkedDates.push(this.obj);
            } else {
              notify(
                {
                  position: {
                    at: 'top centre',
                    my: 'top centre',
                    offset: '0 250',
                  },
                  message: data.message,
                },
                'warning',
                2000
              );
              setTimeout(() => {
                this.router.navigate([this.home]);
              }, 3002);
            }
            (err) => console.log(err);
          });
        }
      } else {
        this.http
          .getData(this.formQueryURLMonthlyWeekly())
          .subscribe((data) => {
            this.result.loading = false;
            if (data.status == 'Success') {
              for (var k = 0; k < data.results.length; k++) {
                console.log(data.results);
                this.getSendURL(data.loginRequired, data.payInfoRequired);
                this.results = data.results;
                this.obj = new Bindvalue();
                this.obj.id = this.id + 1;
                this.obj.SaveUrl = data.results[k].SaveUrl;
                this.obj.BuildingId = data.results[k].BuildingId;
                this.objarr.push(this.obj);
              }

              this.obj.LicPlateClass = this.licplate;
              this.LicPlateNumber.push(this.obj);

              this.BuildingResults = data.results;
              localStorage.setItem(
                'BuildingDetails',
                JSON.stringify(this.BuildingResults)
              );
              var points = [];
              for (var i = 0; i < this.BuildingResults.length; i++) {
                var loca = {
                  location:
                    this.BuildingResults[i].Latitude.toString() +
                    ',' +
                    this.BuildingResults[i].Longitude.toString(),
                  tooltip: {
                    text:
                      '<span onclick="(function(){' +
                      'const slides = document.getElementsByClassName(' +
                      "'build'" +
                      ');' +
                      'for (let i = 0; i < slides.length; i++) {' +
                      'const slide = slides[i];' +
                      "slide.style.display = 'none';}" +
                      "var getdivbuilding = 'divbuilding" +
                      this.BuildingResults[i].BuildingId +
                      "';" +
                      "document.getElementById('sbuilding').value=" +
                      this.BuildingResults[i].BuildingId +
                      ';' +
                      "document.getElementById(getdivbuilding).style.display ='block';" +
                      '})();" style="cursor:pointer;font-weight:bold;">' +
                      this.BuildingResults[i].BuildingName.toString() +
                      '</span>',
                    isShown: true,
                  },
                  iconSrc: redpng,
                  onClick: function (e) {
                    var getobj = JSON.parse(
                      localStorage.getItem('BuildingDetails')
                    );
                    console.log(getobj);

                    this.res = getobj.filter(
                      (o) =>
                        o.Latitude === e.location.lat &&
                        o.Longitude === e.location.lng
                    )[0].BuildingId;
                    (
                      document.getElementById('sbuilding') as HTMLInputElement
                    ).value = this.res;

                    const slides = document.getElementsByClassName('build');
                    for (let i = 0; i < slides.length; i++) {
                      const slide = slides[i] as HTMLElement;
                      slide.style.display = 'none';
                    }
                    var getdivbuilding = 'divbuilding' + this.res;
                    document.getElementById(getdivbuilding).style.display =
                      'block';

                    document.querySelector('#target').scrollIntoView({
                      behavior: 'smooth',
                      block: 'start',
                      inline: 'nearest',
                    });
                  },
                };
                this.mapMarkers.push(loca);
              }
            } else {
              notify(
                {
                  position: {
                    at: 'top centre',
                    my: 'top centre',
                    offset: '0 250',
                  },
                  message: data.message,
                },
                'warning',
                2000
              );
              setTimeout(() => {
                this.router.navigate([this.home]);
              }, 3002);
            }
            (err) => console.log(err);
          });
      }
    } else {
      this.router.navigate([this.home]);
    }
  }
  demo() {
    alert('hi');
  }
  addMarker(e) {
    alert('hai');
    // this.mapMarkers.push({ location: e.location });
  }
  showTooltips(ees: any) {
    alert(ees.Latitude);
  }
  license() {
    this.http.getData(this.formQueryURLLicense()).subscribe((data) => {
      this.result.loading = false;
      if (data.status == 'Success') {
        this.licplate = data.LicPlNo;
        this.licplatevisible = false;
        this.obj = new Bindvalue();
        this.obj.LicPlateClass = this.licplate.toUpperCase();
        //this.checkedDates.push(this.obj.LicPlateClass);
        this.LicPlateNumber.push(this.obj.LicPlateClass);
      } else {
        this.licplatevisible = true;
        // notify({
        //   position: { at: 'top centre', my: 'top centre', offset: '0 250'},
        //   message:data.message
        // }, "warning", 2000);
        // setTimeout(() => {
        //  // this.router.navigate([this.home]);
        // }, 3002);
      }
      (err) => console.log(err);
    });
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    //  this.result.loading = false;
  }

  getSendURL(isLoginRequired, payInfoRequired?): void {
    let appURL = '/';
    if (!this.result.notMobileApp) {
      appURL = '/app/';
    }

    if (isLoginRequired == 'no') {
      // if logged in
      if (payInfoRequired == 'yes') {
        this.buttonText = 'Add credit card and book';
        this.sendURL = appURL + 'creditcard';
      } else {
        this.sendURL = appURL + 'confirmation';
      }
    } else {
      this.buttonText = 'Login and book';
      this.sendURL = appURL + 'login';
    }
  }

  //LicensePlate
  formQueryURLLicense(): string {
    let apiURL = this.resultAPI1;
    return apiURL;
  }

  //below for daily calendar
  formQueryURL(): string {
    const month = this.values.getMonth() + 1;
    const bookingDate = `?bookingDate=${this.values.getFullYear()}-${month}-${this.values.getDate()}`;
    const priceType = `&priceType=${this.result.searchTerm.productType}`;
    const startTime = `&startTime=${this.result.searchTerm.time.getHours()}:${this.result.searchTerm.time.getMinutes()}:00`;
    const duration = `&duration=${this.result.searchTerm.duration}`;
    let promoCode = '';
    if (this.result.searchTerm.promoCode) {
      promoCode = `&promoCode=${this.result.searchTerm.promoCode}`;
    }

    let apiURL =
      this.resultAPI +
      bookingDate +
      priceType +
      startTime +
      duration +
      promoCode;
    return apiURL;
  }

  //below for monthly or weekly calendar
  formQueryURLMonthlyWeekly(): string {
    const month = this.result.searchTerm.startDate.getMonth() + 1;
    const bookingDate = `?bookingDate=${this.result.searchTerm.startDate.getFullYear()}-${month}-${this.result.searchTerm.startDate.getDate()}`;
    const priceType = `&priceType=${this.result.searchTerm.productType}`;
    const startTime = `&startTime=${this.result.searchTerm.time.getHours()}:${this.result.searchTerm.time.getMinutes()}:00`;
    const duration = `&duration=${this.result.searchTerm.duration}`;
    let promoCode = '';
    if (this.result.searchTerm.promoCode) {
      promoCode = `&promoCode=${this.result.searchTerm.promoCode}`;
    }
    // const userID = `&userId=2`;

    let apiURL =
      this.resultAPI +
      bookingDate +
      priceType +
      startTime +
      duration +
      promoCode;
    return apiURL;
  }

  backHome() {
    this.router.navigate([this.home]);
  }

  showInfo(message, title) {
    this.popupTitle = title;
    this.popupMessage = message;
    this.popupVisible = true;
  }

  bookSpace() {
    if (this.licplate == null || this.licplate == '') {
      if (this.licplatevisible == false) {
        return;
      }
    }

    this.result.searchResult = this.LicPlateNumber;
    if (this.licplatevisible == false) {
      this.obj.LicPlateClass = this.licplate.toUpperCase();
    } else {
      this.obj.LicPlateClass = this.licplate;
    }

    this.result.calendarValue = 2;
    this.router.navigate([this.sendURL]);
  }

  Selectedbookingvalues() {
    if (this.licplate == null || this.licplate == '') {
      if (this.licplatevisible == false) {
        return;
      }
    }
    if (this.objarr.length > 0) {
      for (var i = 0; i < this.objarr.length; i++) {
        if (this.licplatevisible == false) {
          this.objarr[i].LicPlateClass = this.licplate.toUpperCase();
        } else {
          this.objarr[i].LicPlateClass = this.licplate;
        }
      }
    } else {
      alert('Please select date');
      return;
    }

    var hidderValue = (document.getElementById('sbuilding') as HTMLInputElement)
      .value;
    this.result.searchResult = this.checkedDates;
    this.result.Buildingid = hidderValue;
    this.result.calendarValue = 1;
    this.router.navigate([this.sendURL]);
  }

  valueChanged(data) {
    this.licplate = data.value;
    if (this.licplate != null && this.licplate != '') {
      this.obj.LicPlateClass = this.licplate.toUpperCase();
    } else {
      //alert("Enter Licence Plate Number");
      return false;
    }
  }

  validateLicNo(e) {
    var regex = new RegExp('^[a-zA-Z0-9]+$');
    this.CheckRegex(regex, e);
  }
  CheckRegex(regex, event) {
    var key = String.fromCharCode(
      !event.charCode ? event.which : event.charCode
    );
    if (!regex.test(key)) {
      event.preventDefault();
      return false;
    }
  }

  ChekboxChange(element: any, checkeddate: any) {
    if (element == true) {
      this.checkedDates.push(checkeddate);
      this.notagree = false;
    } else {
      let removeIndex = this.checkedDates.findIndex(
        (itm) => itm === checkeddate
      );

      if (removeIndex !== -1) this.checkedDates.splice(removeIndex, 1);
    }
  }

  get sortData() {
    return this.DateDetails.sort((a, b) => {
      return <any>new Date(a.StartDate) - <any>new Date(b.StartDate);
    });
  }
  onReady(e: any) {
    this.googleMap = e.originalMap;
    this.googleMap.setOptions({
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false,
    });
    if (this.BuildingResults.length > 1) {
      this.googleMap.zoom = 10;
    }
    var centerpont = this.googleMap.center;

    this.centerCoordinates = { lat: centerpont.lat(), lng: centerpont.lng() };
    this.googleMap.setCenter(this.centerCoordinates);
  }
}
export class Bindvalue {
  id: number;
  result: any;
  SaveUrl: string;
  clickurl: string;
  StartDate: Date;
  LicPlateClass: string;
  BuildingId: string;
}
export class loc {
  location: string;
}
