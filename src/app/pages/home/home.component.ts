import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewInit,
} from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { HttpService, AppInfoService } from '../../shared/services';
import { ResultService } from '../../shared/services/result.service';
import { Router } from '@angular/router';
import _ from 'lodash';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../src/environments/environment';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'date-pipe',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public _AppInfoService: AppInfoService;
  public appinfo: string;
  now: Date = new Date();
  selectedDate: Date;
  minDate: Date;
  maxDate: Date;
  minTime: Date = new Date(new Date().setHours(7, 0, 0, 0));
  startTime: Date;
  endTime: Date = new Date(new Date().setHours(18, 0, 0, 0));
  disabledDates: Function = null;
  showTime: boolean = false;
  maxDuration: number = 12;
  productSource = ['Daily', 'Weekly', 'Monthly'];
  productSourceObject: any;
  durationSource: any = [{ hour: 1 }, { hour: 2 }, { hour: 3 }, { hour: 4 }];
  durationDisable: boolean = true;
  timeDisabled: boolean = true;
  collectionOfDates = [];
  // collectionOfDates:any;
  //selectionofdate : string;
  selectionofdate: any;
  bookingData: any = {};
  checkButton: boolean = true;
  search: any;
  ChksearchFlag: boolean = false;
  activeProduct: number;
  timeList: any = [
    '12:30 am',
    '1:00 am',
    '1:30 am',
    '2:00 am',
    '2:30 am',
    '3:00 am',
    '3:30 am',
    '4:00 am',
    '4:30 am',
    '5:00 am',
    '5:30 am',
    '6:00 am',
    '6:30 pm',
    '7:00 am',
    '7:30 am',
    '8:00 am',
    '8:30 am',
    '9:00 am',
    '9:30 am',
    '10:00 am',
    '10:30 am',
    '11:00 am',
    '11:30 am',
    '12:00 pm',
    '12:30 pm',
    '1:00 pm',
    '1:30 pm',
    '2:00 pm',
    '2:30 pm',
    '3:00 pm',
    '3:30 pm',
    '4:00 pm',
    '4:30 pm',
    '5:00 pm',
    '5:30 pm',
    '6:00 pm',
    '6:30 pm',
    '7:00 pm',
    '7:30 pm',
    '8:00 pm',
    '8:30 pm',
    '9:00 pm',
    '9:30 pm',
    '10:00 pm',
    '10:30 pm',
    '11:00 pm',
    '11:30 pm',
    '12:00 am',
  ];
  selectedItem: string = this.timeList[0];

  inputOption: any = { disabled: false };

  // private productSourceAPI:string = 'https://connect.opuspark.com.au/1mp/api/PricingType/GetActive';
  // private productSourceAPI:string = 'https://reqres.in/api/users?page=2';
  //private productSourceAPI:string= 'https://localhost:44344/api/PricingType/GetActive';
  // private productSourceAPI:string =  'https://localhost:44344/api/PricingType/GetActive';
  // private sourceapi:string='/api/PricingType/GetActive';
  private sourceapi: string = '/api/PricingType/GetActiveProducts';
  private productSourceAPI: string = environment.apiUrl + this.sourceapi;
  screen(width) {
    return width < 700 ? 'sm' : 'lg';
  }

  constructor(
    private http: HttpService,
    private router: Router,
    public result: ResultService,
    public _AuthService: AuthService,
    public AppInfoSer: AppInfoService,
    private cd: ChangeDetectorRef
  ) {
    if (!_AuthService.loggedIn) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit(): void {
    this.appinfo = this.AppInfoSer.title;
    this.result.loading = true;
    const dayInSecond: number = 1000 * 60 * 60 * 24;
    const maxDays: number = 14;

    this.search = JSON.parse(sessionStorage.getItem('Flag'));
    if (this.search == 'true') {
      this.ChksearchFlag = true;
    }
    if (this.search == null) {
      this.ChksearchFlag = true;
    }
    if (this.search == 'false') {
      this.ChksearchFlag = false;
    }
    // alert(this.search);
    this.disabledDates = (data) => {
      return data.view === 'month' && this.isWeekend(data.date);
    };

    this.startTime = this.getClosestTime();
    this.selectedItem = this.getMatchingTime(this.startTime);
    this.minDate = this.getMinmumDate();
    // this.selectedDate = this.changeSelectedDayIfIsWeekend(this.minDate);

    this.maxDate = new Date(this.minDate.getTime() + dayInSecond * maxDays);
    this.bookingData = {
      // startDate:this.selectedDate,
      startDate: this.collectionOfDates,
      time: this.startTime,
    };

    this.http.getData(this.productSourceAPI).subscribe((data) => {
      if (data.status == 'Success') {
        this.result.loading = false;
        this.productSourceObject = data.results;
        const productType = _.find(this.productSourceObject, { Name: 'Daily' });
        this.bookingData.productType = productType.Id;
      }

      (err) => console.log(err);
    });
  }
  AfterViewInit() {}

  getMatchingTime(time) {
    let hour = time.getHours();
    let min = '00';
    let ampm = 'am';

    if (time.getMinutes() != 0) {
      min = '30';
    }

    if (hour > 11) {
      hour = hour - 12;
      if (hour == '0') {
        hour = 12;
      }
      ampm = 'pm';
    }

    const returingTime = `${hour}:${min} ${ampm}`;

    return returingTime;
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.

    this.bookingData = {
      // startDate: this.selectedDate,
      startDate: this.collectionOfDates,
      time: this.getClosestTime(),
    };
  }

  getMinmumDate() {
    let now = new Date();
    const h12h = now.getHours();
    const m12h = now.getMinutes();
    let minute = Number('00' + m12h);
    if (!this.isWeekend(now)) {
      if (h12h >= 19 || (h12h === 18 && minute === 30)) {
        now = new Date(now.getTime() + 1000 * 60 * 60 * 24);
      }

      if (this.bookingData.productType == 1) {
        if ((h12h > 9 && h12h <= 18) || (h12h == 9 && m12h > 30)) {
          this.minDate = new Date(now.getTime() + 1000 * 60 * 60 * 24);
          now = new Date(now.getTime() + 1000 * 60 * 60 * 24);
          this.minDate = this.changeSelectedDayIfIsWeekend(now);
          if (this.selectedDate < this.minDate) {
            this.selectedDate = this.minDate;
          }
        } else {
          this.minDate = this.changeSelectedDayIfIsWeekend(now);
        }
      } else {
        this.minDate = this.changeSelectedDayIfIsWeekend(now);
      }
    } else {
      this.minDate = this.changeSelectedDayIfIsWeekend(now);
    }

    return now;
  }

  getClosestTime() {
    const now = new Date();
    now.setMinutes(Math.ceil(now.getMinutes() / 30) * 30);
    let h12h = now.getHours();
    const m12h = now.getMinutes();
    let minute = Number('00' + m12h);
    var timeString = new Date(new Date().setHours(h12h, minute, 0, 0));

    return timeString;
  }

  changeSelectedDayIfIsWeekend(date) {
    // Not used anymore it will return today.
    const day = date.getDay();
    let returningDate = date;
    return returningDate;
  }

  format(value: any) {
    return value + 'hr';
  }

  isWeekend(date) {
    const day = date.getDay();
    return day === 0 || day === 6;
  }

  valueChanged(e: any, type?: string): void {
    var status = false;

    if (type == 'date') {
      //below is for daily calendar
      if (this.bookingData.productType == 1) {
        //this.bookingData.startDate = e.value;
        for (var j = 0; j < this.collectionOfDates.length; j++) {
          var cdate =
            this.collectionOfDates[j].getDate() +
            '/' +
            this.collectionOfDates[j].getMonth() +
            '/' +
            this.collectionOfDates[j].getFullYear();
          var sdate =
            e.value.getDate() +
            '/' +
            e.value.getMonth() +
            '/' +
            e.value.getFullYear();
          if (cdate == sdate) {
            status = true;
            break;
          }
        }
        if (status == false) {
          if (e.value != null) {
            this.collectionOfDates.push(e.value);
          }
        }
        this.bookingData.startDate = this.collectionOfDates;
        // this.bookingData.startDate = this.sortData;
      }
      //below is for weekly and monthly
      if (
        this.bookingData.productType == 2 ||
        this.bookingData.productType == 3
      ) {
        if (e.value != null) {
          this.bookingData.startDate = e.value;
        }
        //alert(JSON.stringify(this.bookingData.startDate));
      }
    }

    if (type == 'time') {
      const time = e.value;
      let setTime = new Date();
      const parts = time.match(/(\d+):(\d+) (am|pm)/);
      if (parts) {
        let hours = parseInt(parts[1]),
          minutes = parseInt(parts[2]),
          tt = parts[3];
        if (tt === 'pm' && hours < 12) hours += 12;
        setTime.setHours(hours, minutes, 0, 0);
      }

      this.bookingData.time = setTime;
      this.maxDuration = 19 - setTime.getHours();
      this.setDuration(setTime);
      this.checkButton = true;

      if (this.checkTime(this.bookingData.time)) {
        this.bookingData.time = setTime;
      } else {
        this.startTime = this.getClosestTime();
        this.bookingData.time = this.getClosestTime();
        this.checkButton = false;
        notify(
          {
            position: { at: 'top centre', my: 'top centre', offset: '0 250' },
            message: 'Please select a valid time.',
          },
          'success',
          2000
        );
      }

      if (this.maxDuration <= 1) {
        this.durationDisable = true;
      } else {
        this.durationDisable = false;
      }
    }
    if (type == 'duration') {
      this.bookingData.duration = e.value;
    }
    if (type == 'code') {
      this.bookingData.promoCode = e.value;
    }
  }
  setDuration(time) {
    this.maxDuration = 19 - time.getHours();
  }

  checkTime(time) {
    const now = new Date();
    const bookingTime = this.bookingData.startDate.setHours(
      time.getHours() + 1,
      time.getMinutes(),
      0,
      0
    );
    const nowTime = now.getTime();
    if (nowTime > bookingTime) {
      return false;
    } else {
      return true;
    }
  }

  productChanged(id: any): void {
    this.bookingData.productType = id;
    this.bookingData.duration = 1;
    this.getMinmumDate();
    this.collectionOfDates = [];
    this.bookingData.startDate = '';
    this.selectedDate = null;
    //this.selectedDate=null;
  }

  durationChanged(hour: any): void {
    this.bookingData.duration = hour;
  }

  removeDate(i) {
    this.collectionOfDates.splice(i, 1);
  }
  // get sortData() {
  //   return this.collectionOfDates.sort((a, b) => {
  //    return a - b;

  //   });
  // }

  findSpace(e) {
    this.result.searchTerm = this.bookingData;

    //this.result.searchTerm=this.selectionofdate;
    // this.result.searchTerm = this.collectionOfDates;
    if (this.result.notMobileApp) {
      this.router.navigate(['/result']);
    } else {
      this.router.navigate(['/app/result']);
    }
  }
}
