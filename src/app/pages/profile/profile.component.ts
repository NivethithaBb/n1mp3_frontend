import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ResultService } from '../../shared/services/result.service';
import notify from 'devextreme/ui/notify';
import { AuthService, HttpService } from '../../shared/services/';
import { environment } from './../../../../src/environments/environment';
import { Router } from '@angular/router';
@Component({
  templateUrl: 'profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  // apiURL: string = 'https://connect.opuspark.com.au/1mp/'
  // apiURL: string = 'https://localhost:44344/'
  apiURL: string = environment.apiUrl;
  apiHistroy = this.apiURL + 'api/User/GetHistory';
  // creditInfo:any = {};
  showCredit: boolean = true;

  constructor(
    private http: HttpService,
    private router: Router,
    public result: ResultService,
    public authService: AuthService
  ) {
    if (!authService.loggedIn) {
      this.router.navigate(['/login']);
    }
  }

  // ngOnChanges(changes: SimpleChanges) {
  //     // only run when property "data" changed
  //     if (changes['showCredit']) {
  //         console.log(this.showCredit)
  //     }
  // }

  ngOnInit() {}

  changeDate(date, type) {
    let splitedDate = date.split(' ');

    if (type == 'date') {
      return splitedDate[0];
    } else if (type == 'time') {
      return splitedDate[1] + splitedDate[2];
    }
  }

  cancelBooking(cancelAPI) {
    console.log(cancelAPI);
    this.result.loading = true;
    this.http.getData(this.apiURL + cancelAPI).subscribe((data) => {
      this.result.loading = false;
      console.log(data);
      if (data.status == 'Success') {
        this.http.getData(this.apiHistroy).subscribe((data) => {
          this.result.loading = false;
          if (data.status == 'Success') {
            this.result.historySource = data.history;
            // console.log('sucess', data)
            // console.log('sucess', this.result.historySource)
            // this.orginData = this.dataSource;
          }

          (err) => console.log(err);
        });

        notify(
          {
            position: { at: 'top centre', my: 'top centre', offset: '0 250' },
            message: data.message,
          },
          'success',
          2000
        );
      } else {
        notify(
          {
            position: { at: 'top centre', my: 'top centre', offset: '0 250' },
            message: data.message,
          },
          'warning',
          2000
        );
        // this.router.navigate([this.home]);
      }
      (err) => {
        this.result.loading = false;
        console.log(err);
      };
    });
  }
}
