import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor() { }

  searchTerm:any;
  searchResult:any;
  loading:boolean = false;
  calendarValue:any;
  
  popupMessage:string;
  popupVisible = false;
  popupTitle:string;
  notMobileApp = true;
  sessionID:string = '';

  historySource:any;
  Buildingid:string="";
  showInfo(message, title) {

    console.log(message, title)
    this.popupTitle = title;
    this.popupMessage = message;
    this.popupVisible = true;
  }
}
