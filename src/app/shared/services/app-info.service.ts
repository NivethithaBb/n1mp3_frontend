import { Injectable } from '@angular/core';

@Injectable()
export class AppInfoService {
  constructor() {}

  public get title() {
    return 'CH Parking';
  }

  public get currentYear() {
    return new Date().getFullYear();
  }
}
