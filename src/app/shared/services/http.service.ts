import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from './auth.service';


import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient, private authService: AuthService) {}

  public postData(api, postData, token?){
      let head = {};
      const secure = this.authService.loggedIn;

      if(token){
        head = { headers: new HttpHeaders({
          'Authorization':token
        })}
       } else if(secure){
        head = this.getHeader();
      }
      return this.http.post<any>(api, postData, head);
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public postData2(api, postData){
      // let head = {};
      // const secure = this.authService.loggedIn;

      // REST API Username: InterOfficeHoldingsPtyLtdREST_Dev
      // REST API Key: 815ff2a869ceef70f9131f2684371449c757b38df8018233977f91ef490374a1
      // For your testing the API Authorization HTTP Header would be: 
      // Authorization: Basic SW50ZXJPZmZpY2VIb2xkaW5nc1B0eUx0ZFJFU1RfRGV2OjgxNWZmMmE4NjljZWVmNzBmOTEzMWYyNjg0MzcxNDQ5Yzc1N2IzOGRmODAxODIzMzk3N2Y5MWVmNDkwMzc0YTE=

      // head = { headers: new HttpHeaders({
      //       'Authorization':'Basic SW50ZXJPZmZpY2VIb2xkaW5nc1B0eUx0ZFJFU1RfRGV2OjgxNWZmMmE4NjljZWVmNzBmOTEzMWYyNjg0MzcxNDQ5Yzc1N2IzOGRmODAxODIzMzk3N2Y5MWVmNDkwMzc0YTE='
      //     })
      //   }
      return this.http.post<any>(api, postData).pipe(catchError(this.handleError));
  }

  public getData(api, token?){
      let head = {};
      const secure = this.authService.loggedIn;
      if(token){
        head = { headers: new HttpHeaders({
          'Authorization':token
        })}
       } else if(secure){
        head = this.getHeader();
      }
      return this.http.get<any>(api, head);
  }

  private getHeader(){
    return { headers: new HttpHeaders({
      'Authorization':(this.authService.userInfo.token)?this.authService.userInfo.token:""
    })};

  }

}
