import { Injectable, EventEmitter } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { ResultService } from '../services/result.service';
import * as sha512 from 'js-sha512';
import { environment } from './../../../../src/environments/environment';

//const defaultPath = '/home';
const defaultPath = '/result';

@Injectable()
export class AuthService {
  private _loggedIn: boolean = this.checkSessionStorage();
  // authoLogin = new EventEmitter<boolean>();

  get loggedIn(): boolean {
    return this._loggedIn;
  }

  
  get userInfoData(): any {
    return this.userInfo;
  }


  private _lastAuthenticatedPath: string = defaultPath;
  set lastAuthenticatedPath(value: string) {
    this._lastAuthenticatedPath = value;
  }

  public userInfo:any;
  public popupVisible = false;
  public popupMessage:string;
  public popupTitle:string;
  
  showSection:boolean = true;

  poupInfo: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router, private http: HttpClient, public result: ResultService) { }

  checkSessionStorage(){
    if (sessionStorage.getItem("userInfo") !== null) {
      this.setSessionStorage();
      return true;
    } else {
      return false;
    }
  }
  setSessionStorage(data?){
    if(data){
      sessionStorage.setItem("userInfo", JSON.stringify(data) );
    }
    this.userInfo = JSON.parse(sessionStorage.getItem("userInfo"));
    // console.log(this.userInfo)

  }

  private postData(api, postData, secure = true){
      let head = {};
      // if(secure){
      //     head = {headers: new HttpHeaders({
      //             'Authorization':(sessionStorage.getItem("Authorization"))?sessionStorage.getItem("Authorization"):""
      //         })};
      // }
      return this.http.post<any>(api, postData);
  }
  
  showInfo(message,title) {
    this.popupMessage = message;
    this.popupTitle = title;
    this.popupVisible = true;
  }
  getPopupInfoEmitter() {
    // console.log('getPopupInfoEmitter')
    return this.poupInfo;
  }

  logIn(login: string, password: string, fromSearch?:string) {
   // const loginAPI = " https://connect.opuspark.com.au/1mp/api/Authentication/login";
    // const loginAPI="https://localhost:44344/api/Authentication/login";
  
   // const sourceapi='Authentication/login';
    const loginAPI=environment.apiUrl+'api/Authentication/login';
    const loginData = {
      "username": login,
      "password": sha512.sha512(password)
    }
    this.postData(loginAPI, loginData).subscribe((data) => {
      // console.log('loginAPI');
      // console.log(data);
      if(data.status == 'Success'){
      //  if(data.results.searchableFlag==true){
        this.loginProcess(data, fromSearch)
     // }
	  } else {this.showSection = true;
        this.poupInfo.emit(data);
        this.result.showInfo(data.message, data.status);
      }
      (err) => console.log(err)
    });
  }

  loginProcess(data, isApp?, hasPage?){
    this._loggedIn = true;
    this.userInfo = data.results;
    sessionStorage.setItem("userInfo", JSON.stringify(data.results) );
    sessionStorage.setItem("Flag",JSON.stringify(data.results.searchableFlag));
    if(isApp == 'app'){
      if(hasPage){
        const redirect = '/app/' + hasPage;
        this.router.navigate([redirect]);
      } else {
        this.router.navigate(['/app/search']);
      }

    } else if(isApp == 'search'){
      
      if(data.payInfoRequired == 'yes'){
        this.router.navigate(['creditcard']);
      } else {
        this.router.navigate(['confirmation']);
      }
    } else {
      if(data.results.searchableFlag =='true'){
      this.router.navigate([this._lastAuthenticatedPath]);
   }
    }

  }

  logOut() {
    this._loggedIn = false;
    this.userInfo = null;
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }
}

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private authService: AuthService, public result: ResultService) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {

    // console.log('canActivate')
    const isLoggedIn = this.authService.loggedIn;
    const isLoginForm = route.routeConfig.path === 'login';
    const isHomePage:boolean = route.routeConfig.path === 'home';
    const isMobileSearchPage:boolean = route.routeConfig.path === 'app/search';
    // const isResultPage:boolean = route.routeConfig.path === 'result';
    const isResultPage:boolean = route.routeConfig.path.includes('result');
    const isRegisterPage:boolean = route.routeConfig.path.includes('register');
    // const isRecoveryPage:boolean = route.routeConfig.path === 'recovery';
    const isRecoveryPage:boolean = route.routeConfig.path.includes('recovery');
    const isResetPage:boolean = route.routeConfig.path.includes('reset');
    const isVerificationPage:boolean = route.routeConfig.path.includes('verification');
    const loginApp:boolean = route.routeConfig.path === 'app/loginapp/:token';
    const loginRedirectApp:boolean = route.routeConfig.path === 'app/page/:page/:token';
    // const isCreditPage:boolean = route.routeConfig.path === 'creditcard';

    
    
    let isUncheckPage:boolean = false ;
    // const isUncheckPage = route.routeConfig.path === 'result';
    // const isUncheckPage:boolean = route.routeConfig.path === 'home' || 'result' || 'register';


    if (isHomePage || isResultPage || isRegisterPage || isResetPage || isVerificationPage || isRecoveryPage || loginApp || isMobileSearchPage || loginRedirectApp){
      isUncheckPage = true;
    }
    if(isLoggedIn && isLoginForm) {
      // console.log(isUncheckPage + ' 1')
      this.router.navigate([defaultPath]);
      this.authService.lastAuthenticatedPath = defaultPath;
      return false;
    }

    if(!isLoggedIn && !isLoginForm && !isUncheckPage) {
      // console.log(isUncheckPage + ' 2')

      if(!isUncheckPage){
        if(this.result.notMobileApp){
          this.router.navigate(['/login']);
        } else {
          this.router.navigate(['/app/search']);
        }
        // console.log(isUncheckPage + ' 3')
        this.router.navigate(['/home']);
      }
    }

    // if(isLoggedIn || isUncheckPage) {
    //   console.log(isUncheckPage + ' 3')
    //   this.authService.lastAuthenticatedPath = route.routeConfig.path;
    // }

    return isLoggedIn || isLoginForm || isUncheckPage;
  }
}
