import { NgModule, Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AuthService, HttpService } from '../../../shared/services';
import { Router } from '@angular/router';
import * as sha512 from 'js-sha512';
import { ResultService } from '../../../shared/services/result.service';
import { environment } from './../../../../../src/environments/environment';

import * as _ from "lodash";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryComponent implements OnInit {
  dataSource: Array<any>;
  // dataSource: any = [{
  //   "Id":1037,
  //   "name": "Weekday Daily",
  //   "bookingStart":"02/12/2020 7:00 AM",
  //   "bookingEnd":"02/12/2020 7:00 PM",
  //   "amount":"$35.00",
  //   "createdDate":"01/10/2020 1:09 PM",
  //   "refund":""
  // },{
  //   "Id":1038,
  //   "name": "Weekday Daily",
  //   "bookingStart":"02/12/2020 7:00 AM",
  //   "bookingEnd":"02/12/2020 7:00 PM",
  //   "amount":"$35.00",
  //   "createdDate":"01/10/2020 1:09 PM",
  //   "refund":""
  // }]

  orginData:any;

  //api = "https://connect.opuspark.com.au/1mp/api/User/GetHistory"
 sourceapi = "api/User/GetHistory";
  // api ="https://localhost:44344/api/User/GetHistory"
  api =environment.apiUrl+this.sourceapi;
  constructor(private http: HttpService, private router: Router, public result: ResultService, public authService: AuthService) {
   }

  ngOnInit() {
   

    // this.result.loading = true;


    // this.http.getData(this.api).subscribe((data) => {
    //   this.result.loading = false;
    //   console.log(data)
    //   console.log(data.history)
    //   if(data.status == 'Success'){
    //     this.result.historySource = data.history;
    //     console.log('sucess', data)
    //     console.log('sucess', this.result.historySource)
    //     // this.orginData = this.dataSource;
    //   }

    //   (err) => console.log(err)
    // })

   
  }

  
  ngAfterViewInit(): void {
   

    this.result.loading = true;


    this.http.getData(this.api).subscribe((data) => {
      this.result.loading = false;
      console.log(data)
      console.log(data.history)
      if(data.status == 'Success'){
        this.result.historySource = data.history;
        console.log('sucess', data)
        console.log('sucess', this.result.historySource)
        // this.orginData = this.dataSource;
      }

      (err) => console.log(err)
    })
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }

  // logEvent(e){
  //   // console.log(e);
  //   // console.log(this.dataSource)
  //   // console.log(this.dataSource)

  //   // let test = _.without(this.orginData, this.dataSource);
  //   // console.log(this.dataSource)


  //   if(e = 'RowRemoved'){
  //     console.log(e);


  //     let deletedData; // = _.without(this.orginData, this.dataSource);
  //     this.dataSource.forEach(element => {
  //       deletedData = _.without(this.orginData, element);
  //     });
  //     if(deletedData.length > 0){
  //       console.log('this.orginData', this.orginData)
  //       console.log('this.dataSource', this.dataSource)
  //       console.log('deletedData', deletedData)
  //       this.orginData = this.dataSource;
  //       console.log('this.orginData - changed', this.orginData)
  //     }
  //   }
  // }

}
