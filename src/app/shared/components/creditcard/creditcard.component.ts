import { Input, Component, ViewChild, OnInit, Output,EventEmitter } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import { AuthService, HttpService } from '../../services/';
import { Router } from '@angular/router';
import * as sha512 from 'js-sha512';
import { ResultService } from '../../services/result.service';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../../src/environments/environment';

@Component({
  selector: 'app-creditcard',
  templateUrl: './creditcard.component.html',
  styleUrls: ['./creditcard.component.scss']
})
export class CreditcardComponent implements OnInit {
  
  @Input('disableForm') disableForm: boolean;
  @Output('creditInfo') creditInfo: EventEmitter<any> = new EventEmitter()
  @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent
  credit:any = {};
  cardSend:any = {card : '' }

  buttonOptions: any = {
    text: "BOOK NOW",
    type: "success",
    useSubmitBehavior: true
  }

  monthList:any = ["01","02","03","04","05","06","07","08","09","10","11","12"]
  yearList:any = [];
  showForm:boolean = true;

  colCountByScreen2:object;
 // apiURL: string = 'https://connect.opuspark.com.au/1mp/';
  getCURL: string = 'api/Booking/CreditSession';
  // apiURL: string ='https://localhost:44344/';
  apiURL: string = environment.apiUrl;
  cURL: string;
  popupMessage:string;
  popupVisible = false;
  popupTitle:string;

  formDisable:boolean = false;
  inputOption:any = { disabled: false, placeholder: 'Cardholder name' };
  creditCardPattern: any = /^[0-9_\s.-]{13,16}$/;

  inputOption2:any = {
    disabled: false, placeholder: 'Credit card number',
    onValueChanged: e => {
      // console.log('test', e)
      this.changeCreditCard(e.value)
    }
    // mask: '^[0-9_.-]*$',
    // useMaskedValue: true
  };
  disableDropDown:boolean = false;

  apiCC: string =  this.apiURL + "api/User/GetPayInfo" //get
  apiRemoveCC = this.apiURL + "api/User/RemovePayInfo" //get
  showCredit:boolean = true;

  constructor(private http: HttpService, private router: Router, public result: ResultService, public authService: AuthService) {
    this.colCountByScreen2 = {
      xs: 2,
      sm: 2,
      md: 2,
      lg: 2
    };
   }

   changeCreditCard(number){
    //  console.log(number)
     number = number.replaceAll('-','').replaceAll(' ','')
     this.credit.CreditCardNumber = number
   }
   
  ngOnInit() {
    if(this.disableForm){
      this.inputOption = { disabled: true };
      this.inputOption2 = { disabled: true };
      this.disableDropDown = true;
      this.buttonOptions = {
        text: "DELETE",
        type: "success",
        useSubmitBehavior: true
      }
      this.getCCInfo();
    }
    
    // if(this.creditInfo){
    //   this.credit = this.creditInfo;
    // }

    // returns cURL
   
    
    this.generateYearList();
  }

  
  getCCInfo(){
    this.result.loading = false;
    this.http.getData(this.apiCC).subscribe((data) => {
      this.result.loading = false;
      // console.log(data)
      if(data.message == 'Payment information Found' && data.status == "Success"){
        // console.log("getCCInfo")
        // console.log(data)
        this.insertCreditCardInfo(data.results);
      } else {
        this.showForm = false;

        // notify({
        //   position: { at: 'top centre', my: 'top centre', offset: '0 250'},
        //   message:data.message
        // }, "warning", 2000);
      }
      (err) =>{
        console.log(err)
      }
    });

  }

  
  insertCreditCardInfo(result){
    console.log('setCreditInfo(data){')
    console.log(result)

    this.credit.Name = result.cardHolderName
    this.credit.CreditCardNumber = result.creditCardMask;
    this.credit.month = result.dateExpiryMonth
    this.credit.year = result.dateExpiryYear

    // this.credit = result;
    this.credit.CreditCardNumber = result.creditCardMask
  }

  generateYearList(){
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    let year = currentYear - 2000;

    for (let i = 0; i < 8; i++) {
      this.yearList.push(year.toString())
      year = year + 1;
    }

  }

  submitForm(e){
    const newCredit = this.credit.CreditCardNumber.replaceAll('-','')
    this.credit.CreditCardNumber = newCredit;
    
    if(this.disableForm){
      this.removeCCCall();
    } else {
      this.getCURLCall();
    }
    

    e.preventDefault();
  }

  removeCCCall(){
    this.result.loading = true;
    this.http.getData(this.apiRemoveCC).subscribe((data) => {
      this.result.loading = false;
      console.log(data)
      this.showForm = false;
      this.credit = {};
      if( data.status == "Success"){
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "success", 2000);
      } else {
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "warning", 2000);
      }
      (err) =>{
        console.log(err)
      }
    });

  }

  getCURLCall(){
    this.result.loading = true;
    
    this.cardSend.card = {
      cardHolderName: this.credit.Name,
      cardNumber: this.credit.CreditCardNumber,
      dateExpiryMonth: this.credit.month,
      dateExpiryYear: this.credit.year,
      cvc2: this.credit.CVC,
    }
    // console.log(this.cardSend)

    if(this.result.searchResult){

      this.http.getData(this.apiURL + this.getCURL).subscribe((data) => {
        console.log(data)
        if(data.id){
          this.result.sessionID = data.id;
          this.cURL = data.href;
          this.sendCreditInfo(data.href, this.cardSend)
           
          
        } else {
          this.result.loading = false;
        }
      (err) => console.log(err)
    });

      
      // apiURL
      
     
    }

  }

  sendCreditInfo(curl, cardinfo){
    console.log(curl);
    console.log(cardinfo);
    try{
      
      this.http.postData2(curl, cardinfo).subscribe((data) => {
        console.log(data.links[0].href)
        console.log(data.links[0].href.includes('http://app.opusplace.online/n1mp/approved'))

        // 
        if(data.links[0].href.includes('http://app.opusplace.online/n1mp/approved')){
          this.result.loading = false;
          
          if(this.result.notMobileApp){
            // console.log("1")
            // console.log(data)
            this.router.navigate(['confirmation']);
          } else if(!this.result.notMobileApp){
            // console.log("2")
            // console.log(data)
            this.router.navigate(['app/confirmation']);
          }

        } else {
          this.result.loading = false;
          if(data.links[0].href.includes('http://app.opusplace.online/n1mp/declined')){
            // notify("Card declined", "error", 5000);
            notify({
              position: { at: 'top centre', my: 'top centre', offset: '0 250'},
              message:"Unable to process payment. Please check your card details and try again."
            }, "error", 2000);
          } else {
            notify({
              position: { at: 'top centre', my: 'top centre', offset: '0 250'},
              message:"Card cancelled"
            }, "warning", 2000);
          }
        }
        (err) =>{
          alert('error');
          console.log(err)

        } 
      });

    } catch(error){
      console.log('error');
      console.log(error);
    }

  }

  showInfo(message, title) {
    this.popupTitle = title;
    this.popupMessage = message;
    this.popupVisible = true;
  }

  asyncValidation(e) {
    console.log(e)
    // alert(e)
  }

}


// {"card":{"cardHolderName":"Tatum Lindsey","cardNumber":"4999999999999103","dateExpiryMonth":"01","dateExpiryYear":"2021","cvc2":"111"}}


// {
//   "card": {
//     "cardHolderName": "Gary3",
//     "cardNumber": "4999-9999 9999 9103",
//     "dateExpiryMonth": "01",
//     "dateExpiryYear": "21",
//     "cvc2": "111"
//   }
// }
