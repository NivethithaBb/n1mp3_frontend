import { Component, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AuthService, AppInfoService } from '../../services';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxCheckBoxModule } from 'devextreme-angular/ui/check-box';
import { DxTextBoxModule } from 'devextreme-angular/ui/text-box';
import { DxValidatorModule } from 'devextreme-angular/ui/validator';
import { DxValidationGroupModule } from 'devextreme-angular/ui/validation-group';
import { DxDataGridModule, DxFormModule, DevExtremeModule } from 'devextreme-angular';
import { Router } from '@angular/router';
import { ResultService } from '../../../shared/services/result.service';
import { HttpService } from '../../../shared/services';
import { environment } from './../../../../../src/environments/environment';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  login = '';
  password = '';
  popupInfo:any = {
    status:"",
    message:""
  };
  popupVisible:boolean = false;
  isFromSearch:string;
  
  popUP:any;

  buttonOptions: any = {
    text: "Login",
    type: "success",
    useSubmitBehavior: true
  }
 // apiURL: string = 'https://connect.opuspark.com.au/1mp/';
  // apiURL: string ='https://localhost:44344/';
  apiURL: string =environment.apiUrl;
  constructor(private http: HttpService, public authService: AuthService, public appInfo: AppInfoService, private router: Router, public result: ResultService) { }
  
  ngOnInit() {
      this.authService.showSection = true;
    if(this.result.searchResult){
      // console.log(this.result);
      const url = this.apiURL + this.result.searchResult.SaveUrl
      this.http.getData(url).subscribe((data) => {
        // this.result.loading = false;
        if(data.status == 'Success'){
          // console.log(data.results);
          this.result.searchResult.SaveUrl = data.results.Payload;
          // console.log( this.result.searchResult);
          this.isFromSearch = 'search';
        }
        (err) => console.log(err)
      });
    }

  }

  onLoginClick(args?) {
    this.authService.showSection = false;
    // if (!args.validationGroup.validate().isValid) {
    //   return;
    // }

    this.authService.logIn(this.login, this.password, this.isFromSearch);
    // this.popupMessage = this.authService.popupMessage;
    // this.popupTitle = this.authService.popupTitle;
    // this.popupVisible = this.authService.popupVisible;

    if(args.validationGroup){
      args.validationGroup.reset();
     }
    // this.router.navigate(['/home']);
  }

  onCreateAccount(e){
    this.router.navigate(['']);
  }

}
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DxButtonModule,
    DxCheckBoxModule,
    DxTextBoxModule,
    DxValidatorModule,
    DxValidationGroupModule,
    DevExtremeModule
  ],
  declarations: [ LoginFormComponent ],
  exports: [ LoginFormComponent ]
})
export class LoginFormModule { }
