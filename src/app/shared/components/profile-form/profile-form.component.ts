import { NgModule, Component, ViewChild, OnInit } from '@angular/core';
import { DxFormComponent } from 'devextreme-angular';
import { AuthService, HttpService } from '../../../shared/services';
import { Router } from '@angular/router';
import * as sha512 from 'js-sha512';
import { ResultService } from '../../../shared/services/result.service';
import notify from 'devextreme/ui/notify';
import { environment } from './../../../../../src/environments/environment';
@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})
export class ProfileFormComponent implements OnInit {

  @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent
  customer: any;
  colCountByScreen: object;

  password = "";
  passwordOptions: any = {
      mode: "password",
      value: this.password
  };
  customerDataSend:any;

  buttonOptions: any = {
    text: "Edit",
    type: "success",
    useSubmitBehavior: true
  }
  //passwordPattern: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
  passwordPattern: any = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[@~!#$%^&*()\-=_+;:"'?])[A-Za-z\d@~!#$%^&*()\-=_+;:"'?]{8,}$/;

  //apiURL: string = 'https://connect.opuspark.com.au/1mp/api/User/Update';
  apisourceURL: string='api/User/Update';
  //apiURL: string ='https://localhost:44344/api/User/Update';
  apiURL: string =environment.apiUrl+this.apisourceURL;

  constructor(private http: HttpService, private router: Router, public result: ResultService, public authService: AuthService) { }

  ngOnInit() {

    this.customer = {
      Email:  this.authService.userInfo.email,
      FirstName: this.authService.userInfo.firstname,
      LastName: this.authService.userInfo.lastname,
      Mobile: this.authService.userInfo.mobile,
      licePlate: this.authService.userInfo.licPlate,
      password: '',
      repassword: '',
      accepted: false
    };
    this.colCountByScreen = {
      xs: 1,
      sm: 2,
      md: 2,
      lg: 2
    };
    
  }
  
  submitForm(e){
    this.result.loading = true;
    this.customerDataSend = {
      firstName: this.customer.FirstName,
      lastName:  this.customer.LastName,
      mobile: this.customer.Mobile,
      email:this.customer.Email,
      licePlate:this.customer.licePlate,
      password:""
    }
    
    if(this.customer.password){
      this.customerDataSend.password = sha512.sha512(this.customer.password)
    }

    this.http.postData(this.apiURL, this.customerDataSend).subscribe((data) => {
      this.result.loading = false;
      if(data.status == 'Success'){
        // this.result.showInfo(data.message, data.status);
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "success", 2000);
        this.authService.setSessionStorage(data.results);
        setTimeout(function(){
          this.router.navigate(['/home']);
        }, 4010);

        
      } else {
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "warning", 2000);
        // this.result.showInfo(data.message, data.status)
      }
      (err) => console.log(err)
    });


    e.preventDefault();
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
      // this.result.loading = false;
    
  }


  passwordComparison = () => {
    return this.customer.password;
  };
  checkComparison() {
      return true;
  }
}
