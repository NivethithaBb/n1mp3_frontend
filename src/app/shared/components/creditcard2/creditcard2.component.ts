import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-creditcard2',
  templateUrl: './creditcard2.component.html',
  styleUrls: ['./creditcard2.component.scss']
})
export class Creditcard2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  

  screen(width) {
    return ( width < 700 ) ? 'sm' : 'lg';
  }

}
