import { Component, NgModule, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthService } from '../../services';
import { UserPanelModule } from '../user-panel/user-panel.component';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxToolbarModule } from 'devextreme-angular/ui/toolbar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  @Output()
  menuToggle = new EventEmitter<boolean>();

  @Input()
  menuToggleEnabled = false;

  @Input()
  title: string;

  // userMenuItems:any;

  userMenu = [{
    text: 'Profile',
    // icon: 'user',
    onClick: () => {
      this.router.navigate(['/profile']);
    }
  }, {
    text: 'Logout',
    // icon: 'runner',
    onClick: () => {
      this.authService.logOut();
    }
  }];

  
  userMenuItems = [{
    text: 'Login',
    // icon: 'runner',
    onClick: () => {
      this.router.navigate(['/login']);
    }
  }];

  constructor(private authService: AuthService, private router: Router) {
   }

  ngOnInit() {
    // console.log(this.authService.userInfo)
    if (this.authService.userInfo){
      // console.log(this.authService.userInfo)
      this.userMenuItems = this.userMenu;
    }
  }

  toggleMenu = () => {
    this.menuToggle.emit();
  }
  moveHome(e){
    this.router.navigate(['/home']);
  }
  isAutorized() {
    return this.authService.loggedIn;
  }

}

@NgModule({
  imports: [
    CommonModule,
    DxButtonModule,
    UserPanelModule,
    DxToolbarModule
  ],
  declarations: [ HeaderComponent ],
  exports: [ HeaderComponent ]
})
export class HeaderModule { }
