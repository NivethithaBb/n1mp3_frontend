import { Component, HostBinding } from '@angular/core';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { ResultService } from './shared/services/result.service';
import { Router } from '@angular/router';
import { environment } from './../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  @HostBinding('class') get getClass() {
    return Object.keys(this.screen.sizes).filter(cl => this.screen.sizes[cl]).join(' ');
  }
  path: any;

  constructor(private authService: AuthService, private router: Router, public result: ResultService, private screen: ScreenService, public appInfo: AppInfoService) { }

  isAutorized() {
    return this.authService.loggedIn;
  }

  notMobileApp(){
    if (this.router.url.includes('/app')) {
      this.result.notMobileApp = false;
      return false;
    } else {
      this.result.notMobileApp = true;
      return true;
    }
  }
}
