import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import {
  SideNavOuterToolbarModule,
  SideNavInnerToolbarModule,
  SingleCardModule,
} from './layouts';
import { FooterModule, LoginFormModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { AppRoutingModule } from './app-routing.module';
// import { DxAccordionModule, DxCheckBoxModule, DxSliderModule, DxTagBoxModule, DxTemplateModule, DxButtonModule } from 'devextreme-angular';
import { DevExtremeModule } from 'devextreme-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ResultComponent } from './pages/result/result.component';
import { RegisterComponent } from './pages/register/register.component';
import { ConfirmationComponent } from './pages/confirmation/confirmation.component';
import { ResetComponent } from './pages/reset/reset.component';
import { VerificationComponent } from './pages/verification/verification.component';
import { RecoveryComponent } from './pages/recovery/recovery.component';
import { LoginAppComponent } from './pages/login-app/login-app.component';
import { CreditCardComponent } from './pages/credit-card/credit-card.component';

import { ProfileFormComponent } from './shared/components/profile-form/profile-form.component';
import { SearchComponent } from './shared/components/search/search.component';
import { RegisterFormComponent } from './shared/components/register-form/register-form.component';
import { ResetFormComponent } from './shared/components/reset-form/reset-form.component';
import { ProfileMobileComponent } from './mobile-app/pages/profile-mobile/profile-mobile.component';
import { RedirectComponent } from './mobile-app/pages/redirect/redirect.component';

// import { Creditcard2Component } from './shared/components/creditcard2/creditcard2.component';
// import { CreditcardComponent } from './shared/components/creditcard/creditcard.component';
// import { HistoryComponent } from './shared/components/history/history.component';

@NgModule({
  declarations: [
    AppComponent,
    ResultComponent,
    RegisterComponent,
    ConfirmationComponent,
    ResetComponent,
    VerificationComponent,
    RecoveryComponent,
    LoginAppComponent,
    RedirectComponent,
    // Creditcard2Component,
    // CreditCardComponent,
    // ProfileMobileComponent,
    // PopupComponent,
    // CreditcardComponent,
    // HistoryComponent
  ],
  imports: [
    BrowserModule,
    SideNavOuterToolbarModule,
    SideNavInnerToolbarModule,
    SingleCardModule,
    HttpClientModule,
    // HttpClient,
    FooterModule,
    LoginFormModule,
    DevExtremeModule,
    AppRoutingModule,
    NgbModule,
  ],
  providers: [AuthService, ScreenService, AppInfoService, NgbModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
