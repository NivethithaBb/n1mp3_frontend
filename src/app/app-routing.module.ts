import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { ResultComponent } from './pages/result/result.component';
import { TasksComponent } from './pages/tasks/tasks.component';
import { DxDataGridModule, DxFormModule, DevExtremeModule } from 'devextreme-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './pages/register/register.component';
import { ConfirmationComponent } from './pages/confirmation/confirmation.component';
import { ResetComponent } from './pages/reset/reset.component';
import { VerificationComponent } from './pages/verification/verification.component';
import { RecoveryComponent } from './pages/recovery/recovery.component';
import { LoginAppComponent } from './pages/login-app/login-app.component';
import { RedirectComponent } from './mobile-app/pages/redirect/redirect.component';
import { CreditCardComponent } from './pages/credit-card/credit-card.component';



import { CreditcardComponent } from './shared/components/creditcard/creditcard.component';
import { HistoryComponent } from './shared/components/history/history.component';
import { ProfileFormComponent } from './shared/components/profile-form/profile-form.component';
import { SearchComponent } from './shared/components/search/search.component';
import { RegisterFormComponent } from './shared/components/register-form/register-form.component';
import { ResetFormComponent } from './shared/components/reset-form/reset-form.component';


import { ProfileMobileComponent } from './mobile-app/pages/profile-mobile/profile-mobile.component';

// RECOMMENDED
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TabsModule } from 'ngx-bootstrap/tabs';

const routes: Routes = [
  {
    path: 'tasks',
    component: TasksComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/profile',
    component: ProfileMobileComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/search',
    component: HomeComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'creditcard',
    component: CreditCardComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/creditcard',
    component: CreditCardComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/register',
    component: RegisterComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'result',
    component: ResultComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/result',
    component: ResultComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'confirmation',
    component: ConfirmationComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/confirmation',
    component: ConfirmationComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'reset/:token',
    component: ResetComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'verification/:token',
    component: VerificationComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/loginapp/:token',
    component: LoginAppComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/page/:page/:token',
    component: RedirectComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'recovery',
    component: RecoveryComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'app/recovery',
    component: RecoveryComponent,
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'login',
    component: LoginFormComponent,
    canActivate: [ AuthGuardService ]
  },
  // {
  //   path: '',
  //   redirectTo: 'home',
  //   canActivate: [ AuthGuardService ]
  // },
  {
    path: '**',
    //redirectTo: 'home'
    redirectTo:'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), DxDataGridModule, DxFormModule, DevExtremeModule, NgbModule, BrowserAnimationsModule, AccordionModule, BsDatepickerModule.forRoot(), TimepickerModule.forRoot(), TabsModule],
  providers: [AuthGuardService],
  exports: [RouterModule],
  declarations: [
    HomeComponent,
    ProfileComponent,
    ProfileMobileComponent,
    TasksComponent,
    CreditcardComponent,
    CreditCardComponent,
    HistoryComponent,
    ProfileFormComponent,
    SearchComponent,
    RegisterFormComponent,
    ResetFormComponent,
  ]
})
export class AppRoutingModule { }
