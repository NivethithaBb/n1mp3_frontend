
import { NgModule, Component, OnInit, ViewChild, enableProdMode } from '@angular/core';
import { ResultService } from '../../../shared/services/result.service';
import { DxFormComponent } from 'devextreme-angular';
import { HttpService, AuthService } from '../../../shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import notify from 'devextreme/ui/notify';

import * as sha512 from 'js-sha512';

@Component({
  selector: 'app-profile-mobile',
  templateUrl: './profile-mobile.component.html',
  styleUrls: ['./profile-mobile.component.scss']
})
export class ProfileMobileComponent implements OnInit {
  apiURL: string = 'http://connect.opusplace.online/n1mp3/'

  apiHistroy = this.apiURL + "api/User/GetHistory"

  constructor(private http: HttpService, public result: ResultService, public authService: AuthService) {
  }

  ngOnInit() {
  }

  changeDate(date, type){

    let splitedDate = date.split(" ");

    if(type == 'date'){
      return splitedDate[0]
    } else if(type=='time'){
      return (splitedDate[1] + splitedDate[2])
    }


  }

  cancelBooking(cancelAPI){

    console.log(cancelAPI)
    this.result.loading = true;
    this.http.getData(this.apiURL + cancelAPI).subscribe((data) => {
      this.result.loading = false;
      console.log(data)
      if(data.status == 'Success'){
        this.http.getData(this.apiHistroy).subscribe((data) => {
          this.result.loading = false;
          if(data.status == 'Success'){
            this.result.historySource = data.history;
            // console.log('sucess', data)
            // console.log('sucess', this.result.historySource)
            // this.orginData = this.dataSource;
          }
    
          (err) => console.log(err)
        })

        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "success", 2000);
      } else {
        notify({
          position: { at: 'top centre', my: 'top centre', offset: '0 250'},
          message:data.message
        }, "warning", 2000);
        // this.router.navigate([this.home]);
      }
      (err) =>{
        this.result.loading = false;
        console.log(err)
      }
    });


  }

}
