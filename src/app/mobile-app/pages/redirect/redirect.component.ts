  import { NgModule, Component, OnInit, ViewChild, enableProdMode } from '@angular/core';
  import { DxFormComponent } from 'devextreme-angular';
  import { HttpService, AuthService } from '../../../shared/services';
  import { Router, ActivatedRoute } from '@angular/router';
  import { ResultService } from '../../../shared/services/result.service';
  import * as sha512 from 'js-sha512';
  import notify from 'devextreme/ui/notify';
  
  @Component({
    selector: 'app-redirect',
    templateUrl: './redirect.component.html',
    styleUrls: ['./redirect.component.scss']
  })
  export class RedirectComponent implements OnInit {
  
    @ViewChild(DxFormComponent, { static: false }) form:DxFormComponent
    info: any;
    customerDataSend:any;
    colCountByScreen: object;
  
    buttonOptions: any = {
      text: "Verifiy",
      type: "success",
      useSubmitBehavior: true
    }
    popupVisible = false;
    popupMessage:string;
    popupTitle:string;
  
    apiURL: string = 'http://connect.opusplace.online/n1mp3/api/Authentication/MobileAppAuth/'
  
    constructor(private http: HttpService, private authService: AuthService, private router: Router,  private activateRoute: ActivatedRoute, public result: ResultService) {
    }
  
    ngOnInit(): void {
      //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
      //Add 'implements OnInit' to the class.
      this.activateRoute.params.subscribe(params => {
        this.result.loading = true;
        this.info = {
          token : params['token']
        }
        const redirectURL = params['page']
        // const newApiUrl = `${this.apiURL}${params['token']}`
        this.http.getData(this.apiURL, params['token']).subscribe((data) => {
          if(data.status == 'Success'){
            this.authService.loginProcess(data, 'app', redirectURL)
          } else {
            this.showInfo(data.message, data.status);
          }
          this.result.loading = false;
          (err) => console.log(err)
        });
          
       });
      
    }
    ngAfterViewInit(): void {
      //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
      //Add 'implements AfterViewInit' to the class.
      
    }
  
  
    showInfo(message,title) {
      this.popupMessage = message;
      this.popupTitle = title;
      this.popupVisible = true;
    }
  
  }
