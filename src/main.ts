import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { AppInfoService } from './app/shared/services';


if (environment.production) {
  enableProdMode();
}
const appinfo = new AppInfoService();
document.getElementById('hdtitle').innerText = appinfo.title;
platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch((err) => console.error(err));
